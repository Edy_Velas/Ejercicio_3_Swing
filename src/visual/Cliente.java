package visual;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;

/**
 * Created by Baldemar on 15/07/2017.
 */
public class Cliente {
    private JPanel contentPane;
    private JTextField nombre;
    private JTable table1;
    private JButton adicionarButton;
    private JTextField direccion;
    private JButton eliminarButton;
    private JTextField telefono;
    private DefaultTableModel model;

    public Cliente() {



        //Table MOdel
        model=new DefaultTableModel();
        model.addColumn("Nombre");
        model.addColumn("Direccion");
        model.addColumn("Telefono");
        table1.setModel(model);




        adicionarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String nombreVar=nombre.getText();
                String dirVar=direccion.getText();
                int telvar=Integer.parseInt(telefono.getText());
                nombre.setText("");
                direccion.setText("");
                model.addRow(new Object[]{nombreVar,dirVar,telvar});
            }
        });
        eliminarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.removeRow(table1.getSelectedRow());
            }
        });

    }

}
