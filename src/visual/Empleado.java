package visual;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Baldemar on 15/07/2017.
 */
public class Empleado {
    private JPanel contentPane;
    private JTextField nombre;
    private JTable table1;
    private JButton adicionarButton;
    private JTextField dpi;
    private JButton eliminarButton;
    private JTextField telefono;
    private DefaultTableModel model;

    public Empleado() {



        //Table MOdel
        model=new DefaultTableModel();
        model.addColumn("Nombre");
        model.addColumn("DPI");
        model.addColumn("Telefono");
        table1.setModel(model);




        adicionarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String nombreVar=nombre.getText();
                int dpiVar=Integer.parseInt(dpi.getText());
                int telVar=Integer.parseInt(telefono.getText());
                nombre.setText("");
                dpi.setText("");
                model.addRow(new Object[]{nombreVar,dpiVar,telVar});
            }
        });
        eliminarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.removeRow(table1.getSelectedRow());
            }
        });


    }

}
