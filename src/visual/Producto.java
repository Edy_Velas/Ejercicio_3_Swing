package visual;

import com.sun.xml.internal.ws.client.ClientTransportException;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;


public class Producto extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JTextField precio;
    private JTable table1;
    private JButton adicionarButton;
    private JTextField nombre;
    private JButton eliminarButton;
    private DefaultTableModel model;


    public Producto() {

        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        //Table MOdel
        model=new DefaultTableModel();
        model.addColumn("Nombre");
        model.addColumn("Precio");
        table1.setModel(model);

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        adicionarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String nombreVar=nombre.getText();
                double precioVar=Integer.parseInt(precio.getText());
                nombre.setText("");
                precio.setText("");
                model.addRow(new Object[]{nombreVar,precioVar});
            }
        });
        eliminarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.removeRow(table1.getSelectedRow());
            }
        });

    }

    private void onOK() {
        // add your code here
        //dispose();
        JOptionPane.showMessageDialog(this, "Hola Mundo");
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public static void main(String[] args) {
        Producto dialog = new Producto();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}

